package pl.edu.pja.piotrsukiennik.webkbclassifier.converter;

import org.apache.log4j.Logger;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.springframework.core.convert.converter.Converter;
import pl.edu.pja.piotrsukiennik.webkbclassifier.service.HtmlParserService;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
public class ElementsToStringConverter implements Converter<Elements, List<String>> {

    private static final Logger LOG = Logger.getLogger(ElementsToStringConverter.class);

    public List<String> convert(Elements source) {
        List<String> out = new LinkedList<String>();
        traverse(out,source.first());
        return out;
    }


    public void traverse(List<String> str,Node element){
        for (Node childNode: element.childNodes()){
            if (childNode instanceof TextNode){
                String text = ((TextNode) childNode).getWholeText();
                if (!text.trim().isEmpty()){
                    str.add(((TextNode) childNode).text());
                }
            } else {
                traverse(str,childNode);
            }
        }

    }
}
