package pl.edu.pja.piotrsukiennik.webkbclassifier.service;

import org.apache.commons.math3.distribution.UniformRealDistribution;
import org.apache.log4j.Logger;
import org.deeplearning4j.eval.Evaluation;
import org.deeplearning4j.models.classifiers.lstm.LSTM;
import org.deeplearning4j.models.featuredetectors.rbm.RBM;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.layers.factory.LayerFactories;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.api.activation.Tanh;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.lossfunctions.LossFunctions;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.Dataset;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
public class DeepLearningClassifierServiceImpl implements ClassifierService {
    private static final Logger LOG = Logger.getLogger( DeepLearningClassifierServiceImpl.class );


    private MultiLayerNetwork classifier;

    @Override
    public void init( DataSet dataset ) {
        int dataDimension = dataset.numInputs();
        int labelDimension = dataset.numInputs();
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
         .nIn( dataDimension ).nOut( labelDimension )
         .visibleUnit( RBM.VisibleUnit.GAUSSIAN ).hiddenUnit( RBM.HiddenUnit.RECTIFIED )
         .iterations( 100 )
         .weightInit( WeightInit.DISTRIBUTION )
         .dist( new UniformRealDistribution( 0, 1 ) )
         .activationFunction( new Tanh() ).k( 1 )
         .lossFunction( LossFunctions.LossFunction.RMSE_XENT )
         .learningRate( 1e-1f ).momentum( 0.9 ).regularization( true ).l2( 2e-4 )
         .optimizationAlgo( OptimizationAlgorithm.LBFGS )
         .constrainGradientToUnitNorm( true )
         .layerFactory( LayerFactories.getFactory( LSTM.class ) )
         .list( 2 )
         .hiddenLayerSizes( 3 )
         .build();
        classifier = new MultiLayerNetwork( conf );
        classifier.init();
    }

    public void fit( DataSet train ) {
        classifier.fit( train );
        LOG.info( "Evaluate model...." );
    }

    public void eval( DataSet test ) {
        Evaluation eval = new Evaluation();
        INDArray output = classifier.output( test.getFeatureMatrix() );
        eval.eval( test.getLabels(), output );
        LOG.info( eval.stats() );
    }


}
