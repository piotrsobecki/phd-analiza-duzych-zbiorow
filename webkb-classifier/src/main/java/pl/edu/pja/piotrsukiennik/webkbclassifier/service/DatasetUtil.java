package pl.edu.pja.piotrsukiennik.webkbclassifier.service;

import pl.edu.pja.piotrsukiennik.webkbclassifier.model.DatasetConfig;

import java.util.*;

/**
 * @author Piotr Sukiennik
 * @date 24.05.15
 */
public class DatasetUtil {


    public static Collection<DatasetConfig> folds( String attribute, List<String> alloptions ) {
        return folds(attribute,new ArrayList<String>(),new ArrayList<String>(),alloptions);
    }

    public static Collection<DatasetConfig> folds( String attribute,  List<String> alwaysTrainOptions, List<String> alloptions ) {
        return folds(attribute,alwaysTrainOptions,new ArrayList<String>(),alloptions);
    }

    public static Collection<DatasetConfig> folds( String attribute, List<String> alwaysTrainOptions, List<String> alwaysTestOptions, List<String> alloptions ) {
        Collection<DatasetConfig> configs = new ArrayList<DatasetConfig>();
        for ( int i = 0; i < alloptions.size(); i++ ) {
            String testOption =  alloptions.get( i );
            Set<String> trainCategories = new HashSet<String>();
            Set<String> testCategories = new HashSet<String>();
            trainCategories.addAll( alwaysTrainOptions );
            testCategories.addAll( alwaysTestOptions );
            trainCategories.addAll( alloptions );
            testCategories.add( testOption );
            trainCategories.remove( testOption );
            trainCategories.removeAll( alwaysTestOptions );
            testCategories.removeAll( alwaysTrainOptions );
            configs.add( new DatasetConfig(attribute, trainCategories, testCategories ) );
        }
        return configs;
    }
}
