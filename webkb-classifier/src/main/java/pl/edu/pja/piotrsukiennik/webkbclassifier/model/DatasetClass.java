package pl.edu.pja.piotrsukiennik.webkbclassifier.model;


import java.util.ArrayList;
import java.util.Collection;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
public class DatasetClass<T> {

    private String classification;
    public Collection<T> objects;

    public DatasetClass( String classification) {
       this(classification,new ArrayList<T>());
    }
    public DatasetClass( String classification, Collection<T> objects) {
        this.classification = classification;
        this.objects = objects;
    }


    public String getClassification() {
        return classification;
    }

    public void setClassification( String classification ) {
        this.classification = classification;
    }

    public Collection<T> getObjects() {
        return objects;
    }

    public void setObjects( Collection<T> objects ) {
        this.objects = objects;
    }
}
