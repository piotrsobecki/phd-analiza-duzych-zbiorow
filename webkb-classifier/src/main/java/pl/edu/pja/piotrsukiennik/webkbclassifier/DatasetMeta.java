package pl.edu.pja.piotrsukiennik.webkbclassifier;

import pl.edu.pja.piotrsukiennik.webkbclassifier.model.DatasetConfig;
import pl.edu.pja.piotrsukiennik.webkbclassifier.service.DatasetUtil;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by Piotr on 2015-07-06.
 */
public class DatasetMeta {

    public static final String ATTRIBUTE_CLASS ="TypStrony";

    public static final String RELATION_NAME ="Typystron";

    public static final String ATTRIBUTE_MODIFIER_FILE = "MODIFIER_FILE";

    public static final String ATTRIBUTE_MODIFIER_UNI = "MODIFIER_UNI";

    public static final double ESSENTIAL_WEIGHT = -1;

    public static final Collection<DatasetConfig> DATASET_CONFIGS = DatasetUtil.folds(
            ATTRIBUTE_MODIFIER_UNI,
            Collections.singletonList("misc"),
            Arrays.asList("cornell", "texas", "washington", "wisconsin")
    );
    private DatasetMeta(){

    }
}
