package pl.edu.pja.piotrsukiennik.webkbclassifier.model;

import java.util.Map;

/**
 * Created by Piotr.Sukiennik on 2015-05-26.
 */
public class AnnotatedObject<T> {

    private T object;
    private Map<String,String> modifiers;

    public AnnotatedObject(T object,  Map<String,String> modifiers) {
        this.object = object;
        this.modifiers = modifiers;
    }

    public T getObject() {
        return object;
    }

    public Map<String, String> getModifiers() {
        return modifiers;
    }
}
