package pl.edu.pja.piotrsukiennik.webkbclassifier.converter;

import org.apache.log4j.Logger;
import org.jsoup.select.Elements;
import org.springframework.core.convert.converter.Converter;
import pl.edu.pja.piotrsukiennik.webkbclassifier.service.HtmlParserService;

import java.io.File;
import java.io.IOException;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
public class HtmlConverter implements Converter<File,Elements> {


    private static final Logger LOG = Logger.getLogger( HtmlConverter.class );


    private HtmlParserService htmlParserService;

    public HtmlConverter( HtmlParserService htmlParserService ) {
        this.htmlParserService = htmlParserService;
    }

    public Elements convert( File source ) {
        try {
            return htmlParserService.getElementsWithText( htmlParserService.parseDocument( source ) );
        }
        catch ( IOException e ) {
            throw new RuntimeException( e );
        }
    }
}
