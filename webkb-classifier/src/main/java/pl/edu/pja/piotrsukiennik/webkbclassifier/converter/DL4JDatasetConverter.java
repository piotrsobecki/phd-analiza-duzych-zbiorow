package pl.edu.pja.piotrsukiennik.webkbclassifier.converter;

import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.models.word2vec.wordstore.inmemory.InMemoryLookupCache;
import org.deeplearning4j.text.tokenization.tokenizer.Tokenizer;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.factory.Nd4j;
import org.springframework.core.convert.converter.Converter;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.AnnotatedObject;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.Dataset;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.DatasetClass;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 * Created by Piotr.Sukiennik on 2015-05-26.
 */
public class DL4JDatasetConverter {


    private static final TokenizerFactory TOKENIZER_FACTORY = new DefaultTokenizerFactory();

    private static final Converter<String, String> CLEANER = new WordCleaner();

    private static final int TOP_HOW_MANY = Integer.MAX_VALUE;

    public void addWordToCache( VocabCache vocabCache, VocabWord vocabWord ) {
        vocabCache.addToken( vocabWord );
        vocabCache.addWordToIndex( vocabWord.getIndex(), vocabWord.getWord() );
        vocabCache.putVocabWord( vocabWord.getWord() );
    }

    public void addWordToCache( VocabCache vocabCache, String tokenStr, int count ) {
        String token = CLEANER.convert( tokenStr );
        if ( token != null && !token.isEmpty() ) {
            if ( !vocabCache.hasToken( token ) ) {
                VocabWord vocabWord = new VocabWord( 1, token );
                vocabWord.setIndex( count );
                addWordToCache( vocabCache, vocabWord );
            }
            else {
                vocabCache.incrementWordCount( token );
            }
        }
    }

    public VocabCache buildVocab( Dataset<List<String>> source ) {
        int count = 0;
        VocabCache vocabCache = new InMemoryLookupCache();
        for ( Map.Entry<String, DatasetClass<AnnotatedObject<List<String>>>> classEntry : source.getClasses().entrySet() ) {
            for ( AnnotatedObject<List<String>> annotatedObject : classEntry.getValue().getObjects() ) {
                for ( String string : annotatedObject.getObject() ) {
                    Tokenizer tokenizer = TOKENIZER_FACTORY.create( string );
                    for ( String tokenStr : tokenizer.getTokens() ) {
                        addWordToCache( vocabCache, tokenStr, count++ );
                    }
                }
            }
        }
        return vocabCache;
    }


    /**
     * Sorts tokens in vocab cache and adds to new vocab top TOP_HOW_MANY words with most frequency
     *
     * @param vocabCache
     * @return
     */
    public VocabCache postProcess( VocabCache vocabCache ) {
        VocabCache newVocab = new InMemoryLookupCache();
        List<VocabWord> tokens = new ArrayList<VocabWord>( vocabCache.tokens() );
        Collections.sort( tokens );
        Collections.reverse( tokens );
        for ( int i = 0; i < Math.min( TOP_HOW_MANY - 1, tokens.size() ); i++ ) {
            VocabWord vocabWord = tokens.get( i );
            vocabWord.setIndex( i );
            addWordToCache( newVocab, vocabWord );
        }
        return newVocab;
    }

    public DataSet convert( Dataset<List<String>> source ) {
        VocabCache vocabCache = postProcess( buildVocab( source ) );
        DL4JVectorizer DL4JVectorizer = new DL4JVectorizer( vocabCache );
        List<String> labels = new ArrayList<String>( source.getClasses().keySet() );
        INDArray dataSetArray = Nd4j.create( source.getSize(), vocabCache.numWords() );
        INDArray dataSetLabels = Nd4j.create( source.getSize(), labels.size() );
        int row = 0;
        for ( Map.Entry<String, DatasetClass<AnnotatedObject<List<String>>>> classEntry : source.getClasses().entrySet() ) {
            INDArray labelsVector = DL4JVectorizer.labelVector( labels, classEntry.getKey() );
            for ( AnnotatedObject<List<String>> annotatedObject : classEntry.getValue().getObjects() ) {
                INDArray inputVector = DL4JVectorizer.documentVector( vocabCache, annotatedObject );
                dataSetArray.putRow( row, inputVector );
                dataSetLabels.putRow( row, labelsVector );
                row++;
            }
        }
        return new DataSet( dataSetArray, dataSetLabels );
    }
}
