package pl.edu.pja.piotrsukiennik.webkbclassifier.converter;

import org.apache.commons.lang3.StringUtils;
import org.deeplearning4j.models.word2vec.VocabWord;
import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.models.word2vec.wordstore.inmemory.InMemoryLookupCache;
import org.deeplearning4j.text.tokenization.tokenizer.Tokenizer;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.springframework.core.convert.converter.Converter;
import pl.edu.pja.piotrsukiennik.webkbclassifier.DatasetMeta;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.AnnotatedObject;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.Dataset;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.DatasetClass;
import weka.core.*;

import java.util.*;

/**
 * Created by Piotr.Sukiennik on 2015-05-26.
 */
public class WekaInstancesDatasetConverter {


    private static final TokenizerFactory TOKENIZER_FACTORY = new DefaultTokenizerFactory();

    private static final Converter<String, String> CLEANER = new WordCleaner();

    private static final int TOP_HOW_MANY = 500;

    public void addWordToCache( VocabCache vocabCache, VocabWord vocabWord ) {
        vocabCache.addToken( vocabWord );
        vocabCache.addWordToIndex( vocabWord.getIndex(), vocabWord.getWord() );
        vocabCache.putVocabWord( vocabWord.getWord() );
    }

    public void addWordToCache( VocabCache vocabCache, String tokenStr, int count ) {
        String token = CLEANER.convert( tokenStr );
        if ( token != null && !token.isEmpty() ) {
            if ( !vocabCache.hasToken( token ) ) {
                VocabWord vocabWord = new VocabWord( 1, token );
                vocabWord.setIndex( count );
                addWordToCache( vocabCache, vocabWord );
            }
            else {
                vocabCache.incrementWordCount( token );
            }
        }
    }

    public VocabCache buildVocab( Dataset<List<String>> source ) {
        int count = 0;
        VocabCache vocabCache = new InMemoryLookupCache();
        for ( Map.Entry<String, DatasetClass<AnnotatedObject<List<String>>>> classEntry : source.getClasses().entrySet() ) {
            for ( AnnotatedObject<List<String>> annotatedObject : classEntry.getValue().getObjects() ) {
                for ( String string : annotatedObject.getObject() ) {
                    Tokenizer tokenizer = TOKENIZER_FACTORY.create( string );
                    for ( String tokenStr : tokenizer.getTokens() ) {
                        addWordToCache( vocabCache, tokenStr, count++ );
                    }
                }
            }
        }
        return vocabCache;
    }


    /**
     * Sorts tokens in vocab cache and adds to new vocab top TOP_HOW_MANY words with most frequency
     *
     * @param vocabCache
     * @return
     */
    public VocabCache postProcess( VocabCache vocabCache ) {
        VocabCache newVocab = new InMemoryLookupCache();
        List<VocabWord> tokens = new ArrayList<VocabWord>( vocabCache.tokens() );
        Collections.sort( tokens );
        Collections.reverse( tokens );
        for ( int i = 0; i < Math.min( TOP_HOW_MANY , tokens.size() ); i++ ) {
            VocabWord vocabWord = tokens.get( i );
            vocabWord.setIndex( i );
            addWordToCache( newVocab, vocabWord );
        }
        return newVocab;
    }


    public FastVector datasetAttributes(Dataset dataset, Attribute clazzAttribute, VocabCache vocabCache){
        int numWords = vocabCache.numWords();
        FastVector attributes = new FastVector();
        for (int i =0; i< numWords; i++){
            String word = vocabCache.wordAtIndex(i);
            if (word!=null){
                attributes.addElement(new Attribute(word));
            }
        }
        Set<Map.Entry<String,Set<String>>> modifiers = dataset.getModifierValues().entrySet();
        for (Map.Entry<String,Set<String>> modifier: modifiers){
            attributes.addElement(modifierAttr(modifier.getKey(), modifier.getValue()));
        }
        attributes.addElement(clazzAttribute);
        return attributes;
    }

    public Attribute modifierAttr(String modifier, Set<String> values){
        FastVector fvModifiers = new FastVector(values.size());
        for (String clazz: values){
            fvModifiers.addElement(clazz);
        }
        Attribute attribute = new Attribute(modifier,fvModifiers);
        attribute.setWeight(DatasetMeta.ESSENTIAL_WEIGHT);
        return attribute;
    }

    public Attribute classAttribute(Dataset dataset){
        Set<String> classes = dataset.getClassifications();
        FastVector fvClass = new FastVector(classes.size()+1);
        for (String clazz: classes){
            fvClass.addElement(clazz);
        }
        Attribute attribute = new Attribute(DatasetMeta.ATTRIBUTE_CLASS,fvClass);
        attribute.setWeight(DatasetMeta.ESSENTIAL_WEIGHT);
        return attribute;
    }

    public Instances convert( Dataset<List<String>> source ) {
        VocabCache vocabCache = postProcess( buildVocab( source ) );
        DL4JVectorizer dl4JVectorizer = new DL4JVectorizer( vocabCache );
        Attribute clazzAttribute = classAttribute(source);
        FastVector fvWekaAttributes = datasetAttributes(source, clazzAttribute, vocabCache);
        Instances dataSet = new Instances(DatasetMeta.RELATION_NAME, fvWekaAttributes, source.getSize());
        dataSet.setClass(clazzAttribute);
        dataSet.setClassIndex(fvWekaAttributes.indexOf(clazzAttribute));
        for ( Map.Entry<String, DatasetClass<AnnotatedObject<List<String>>>> classEntry : source.getClasses().entrySet() ) {
            for ( AnnotatedObject<List<String>> annotatedObject : classEntry.getValue().getObjects() ) {
                INDArray inputVector = dl4JVectorizer.documentVector( vocabCache, annotatedObject );
                Instance instance = new Instance(fvWekaAttributes.size());
                instance.setDataset(dataSet);
                for (int column=0; column<inputVector.columns();column++){
                    String word = vocabCache.wordAtIndex(column);
                    if (word!=null){
                        instance.setValue(dataSet.attribute(vocabCache.wordAtIndex(column)), inputVector.getInt(column));
                    }
                }
                for (Map.Entry<String,String> modifier: annotatedObject.getModifiers().entrySet()){
                    instance.setValue(dataSet.attribute(modifier.getKey()), modifier.getValue());
                }
                instance.setClassValue(classEntry.getKey());
                dataSet.add(instance);
            }
        }
        return dataSet;
    }
}
