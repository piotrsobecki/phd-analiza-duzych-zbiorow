package pl.edu.pja.piotrsukiennik.webkbclassifier.service;

import pl.edu.pja.piotrsukiennik.webkbclassifier.model.DatasetConfig;
import weka.classifiers.Classifier;
import weka.core.Instance;
import weka.core.Instances;

import java.util.Collection;

/**
 * Created by Piotr on 2015-07-06.
 */
public interface WekaClassifierService {

    void init(Instances instances) throws Exception;

    void eval(Instances instances, Collection<DatasetConfig> datasetConfigs) throws Exception;

    void eval(Instances instances) throws Exception;

    String classify(Instance instance) throws Exception;

    Classifier getClassifier();
}
