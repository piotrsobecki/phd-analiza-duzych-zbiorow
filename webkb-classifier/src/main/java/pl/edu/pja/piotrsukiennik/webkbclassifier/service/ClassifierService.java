package pl.edu.pja.piotrsukiennik.webkbclassifier.service;

import org.nd4j.linalg.dataset.DataSet;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.Dataset;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
public interface ClassifierService {

    void init(DataSet dataset);
    void fit(DataSet dataset);
    void eval(DataSet dataset);
}
