package pl.edu.pja.piotrsukiennik.webkbclassifier.service;

import org.apache.log4j.Logger;
import pl.edu.pja.piotrsukiennik.webkbclassifier.DatasetMeta;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.DatasetConfig;
import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.meta.FilteredClassifier;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;
import weka.filters.unsupervised.instance.SubsetByExpression;

import java.util.Collection;

/**
 * Created by Piotr on 2015-07-06.
 */
public class WekaClassifierServiceImpl implements WekaClassifierService {

    private static final Logger LOG = Logger.getLogger(WekaClassifierServiceImpl.class);

    private Classifier classifierType;

    private Classifier classifier;

    private Attribute classAttribute;

    private static final String ATT_EXPRESSION_FORMAT = "(ATT%d is '%s')";
    private static final String ATT_EXPRESSION_SEPARATOR = " or ";

    public WekaClassifierServiceImpl(Classifier classifier) {
        this.classifierType = classifier;
    }

    @Override
    public void init(Instances instances) throws Exception {
        LOG.info("Start classifier build");
        long start = System.currentTimeMillis();
        classifier = build(instances);
        classAttribute = instances.classAttribute();
        long end = System.currentTimeMillis();
        LOG.info(String.format("End classifier build (%d msec)",end-start));
    }


    protected Classifier build(Instances instances) throws Exception {
        FilteredClassifier filteredClassifier = new FilteredClassifier();
        Remove remove = new Remove();
        int uniIndex = instances.attribute(DatasetMeta.ATTRIBUTE_MODIFIER_UNI).index();
        int fileIndex = instances.attribute(DatasetMeta.ATTRIBUTE_MODIFIER_FILE).index();
        remove.setAttributeIndices(String.format("%d,%d", fileIndex + 1, uniIndex + 1));
        filteredClassifier.setFilter(remove);
        filteredClassifier.setClassifier(Classifier.makeCopy(classifierType));
        filteredClassifier.buildClassifier(instances);
        return filteredClassifier;
    }

    protected Filter removeWithModifier(Instances instances, Collection<String> modifiers) throws Exception {
        SubsetByExpression removeWithValues = new SubsetByExpression();
        removeWithValues.setInputFormat(instances);
        Attribute modifierAttribute = instances.attribute(DatasetMeta.ATTRIBUTE_MODIFIER_UNI);
        int index = modifierAttribute.index() + 1;
        String expression = "";
        String sep = "";
        for (String modifier : modifiers) {
            expression += sep + String.format(ATT_EXPRESSION_FORMAT, index, modifier);
            sep = ATT_EXPRESSION_SEPARATOR;
        }
        removeWithValues.setExpression(expression);
        return removeWithValues;
    }

    @Override
    public void eval(Instances instances, Collection<DatasetConfig> datasetConfigs) throws Exception {
        LOG.info("Start classifier eval");
        for (DatasetConfig datasetConfig : datasetConfigs) {
            long start = System.currentTimeMillis();
            Instances trainDataset = instances;
            Instances testDataset = instances;

            trainDataset = Filter.useFilter(trainDataset, removeWithModifier(trainDataset, datasetConfig.getTrainSources()));
            testDataset = Filter.useFilter(testDataset, removeWithModifier(testDataset, datasetConfig.getTestSources()));

            LOG.info("Start classifier build for fold");
            Classifier classifier = build(trainDataset);

            long end = System.currentTimeMillis();
            LOG.info(String.format("End classifier  build for fold (%d msec)",end-start));

            Evaluation eTest = new Evaluation(trainDataset);
            eTest.evaluateModel(classifier, testDataset);

            LOG.info(String.format("Evaluation ||| Train instances(%d):%s; Test instances(%d):%s\n%s\n%s", trainDataset.numInstances(), datasetConfig.getTrainSources(), testDataset.numInstances(), datasetConfig.getTestSources(), eTest.toClassDetailsString(), eTest.toSummaryString()));

        }
        LOG.info("End classifier eval");
    }


    @Override
    public void eval(Instances instance) throws Exception {
        LOG.info(String.format("Evaluation on %d instances", instance.numInstances()));
        Evaluation eTest = new Evaluation(instance);
        eTest.evaluateModel(classifier, instance);
        LOG.info(String.format("Evaluation \n%s", eTest.toClassDetailsString()));
        LOG.info(String.format("Evaluation \n%s", eTest.toSummaryString()));
    }


    @Override
    public String classify(Instance instance) throws Exception {
        double[] distribution = classifier.distributionForInstance(instance);
        int ind = Integer.MIN_VALUE;
        double max = Double.MIN_VALUE;
        for (int i = 0; i < distribution.length; i++) {
            if (distribution[i] > max) {
                ind = i;
                max = distribution[i];
            }
        }
        return classAttribute.value(ind);
    }

    @Override
    public Classifier getClassifier() {
        return classifier;
    }
}
