package pl.edu.pja.piotrsukiennik.webkbclassifier.service;

import pl.edu.pja.piotrsukiennik.webkbclassifier.model.Dataset;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.DatasetConfig;

import java.io.File;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
public interface DatasetService<T> {

    Dataset<T> parse( File dir);
}
