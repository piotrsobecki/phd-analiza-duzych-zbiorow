package pl.edu.pja.piotrsukiennik.webkbclassifier.service;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
public interface HtmlParserService {

    Document parseDocument( File file ) throws IOException;
    Elements getElementsWithText( Document doc );
    String getTextFromElement(Element element);
}
