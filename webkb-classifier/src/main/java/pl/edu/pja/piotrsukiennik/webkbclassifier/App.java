package pl.edu.pja.piotrsukiennik.webkbclassifier;


import org.apache.log4j.Logger;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import pl.edu.pja.piotrsukiennik.webkbclassifier.converter.ElementsToStringConverter;
import pl.edu.pja.piotrsukiennik.webkbclassifier.converter.HtmlConverter;
import pl.edu.pja.piotrsukiennik.webkbclassifier.converter.WekaInstancesDatasetConverter;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.DatasetConfig;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.DatasetConverter;
import pl.edu.pja.piotrsukiennik.webkbclassifier.service.*;
import weka.core.Attribute;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.ArffLoader;
import weka.core.converters.ArffSaver;
import weka.filters.Filter;

import javax.annotation.Resource;
import java.io.*;
import java.util.*;

/**
 * Hello world!
 */

public class App {

    private static final Logger LOG = Logger.getLogger(App.class);

    @Value("${data.dir}")
    private String dataDir;

    @Value("${work.dir}")
    private String workDir;

    @Value("${dataset.file}")
    private String datasetFilePath;

    @Value("${dataset.filtered.file}")
    private String datasetFilteredFilePath;

    @Value("${attributes.file}")
    private String attributesFile;

    @Value("${attributes.filtered.file}")
    private String attributesFilteredFile;

    @Resource
    private WekaClassifierService wekaClassifierService;

    @Resource
    private DatasetService<File> datasetService;

    @Resource
    private HtmlParserService htmlParserService;

    @Resource
    private WekaInstancesDatasetConverter wekaInstancesDatasetConverter;


    public void go() throws Exception {

        ArffLoader arffLoader = new ArffLoader();
        ArffSaver arffSaver = new ArffSaver();

        File datasetFile = new File(datasetFilePath);
        File datasetFilteredFile = new File(datasetFilteredFilePath);

        Instances dataSet = null;
        if (datasetFilteredFile.exists()) {
            arffLoader.setFile(datasetFilteredFile);
            dataSet = arffLoader.getDataSet();
        } else {
            if (datasetFile.exists()) {
                arffLoader.setFile(datasetFile);
                dataSet = arffLoader.getDataSet();
            } else {
                HtmlConverter htmlConverter = new HtmlConverter(htmlParserService);
                DatasetConverter<File, Elements> htmlParser = new DatasetConverter<File, Elements>(htmlConverter);

                ElementsToStringConverter toStringConverter = new ElementsToStringConverter();
                DatasetConverter<Elements, List<String>> htmlElementsParser = new DatasetConverter<Elements, List<String>>(toStringConverter);

                dataSet = wekaInstancesDatasetConverter.convert(
                        htmlElementsParser.convert(
                                htmlParser.convert(
                                        datasetService.parse(
                                                new File(dataDir)
                                        )
                                )
                        )
                );
                Attribute classAttribute = dataSet.attribute(DatasetMeta.ATTRIBUTE_CLASS);
                dataSet.setClass(classAttribute);
                dataSet.setClassIndex(classAttribute.index());

                AttributeWeighter attributeWeighter = new AttributeWeighter();
                attributeWeighter.weight(dataSet);
                writeAttributes(new File(attributesFile), dataSet);

                arffSaver.setFile(datasetFile);
                arffSaver.setInstances(dataSet);
                arffSaver.writeBatch();
            }

            Attribute classAttribute = dataSet.attribute(DatasetMeta.ATTRIBUTE_CLASS);
            dataSet.setClass(classAttribute);
            dataSet.setClassIndex(classAttribute.index());

            AttributeWeighter attributeWeighter = new AttributeWeighter();
            attributeWeighter.weight(dataSet);
            dataSet = Filter.useFilter(dataSet, attributeWeighter.onlyTopAttributes(dataSet, 0.10));
            writeAttributes(new File(attributesFilteredFile), dataSet);

            arffSaver.setFile(datasetFilteredFile);
            arffSaver.setInstances(dataSet);
            arffSaver.writeBatch();
        }

        Attribute classAttribute = dataSet.attribute(DatasetMeta.ATTRIBUTE_CLASS);
        dataSet.setClass(classAttribute);
        dataSet.setClassIndex(classAttribute.index());

        wekaClassifierService.eval(dataSet, DatasetMeta.DATASET_CONFIGS);

        wekaClassifierService.init(dataSet);
        wekaClassifierService.eval(dataSet);

        // LOG.info(wekaClassifierService.getClassifier().toString());
        //testAll(dataSet);
    }



    public void testAll(Instances dataSet) throws Exception {
        Enumeration<Instance> instanceEnumeration = dataSet.enumerateInstances();
        do {
            Instance instance = instanceEnumeration.nextElement();
            String uni = instance.stringValue(dataSet.attribute(DatasetMeta.ATTRIBUTE_MODIFIER_UNI));
            String file = instance.stringValue(dataSet.attribute(DatasetMeta.ATTRIBUTE_MODIFIER_FILE));
            String clazz = instance.stringValue(dataSet.classAttribute());
            String clazzClass = wekaClassifierService.classify(instance);
            boolean valid = clazz.equals(clazzClass);
            String msg = String.format("%b :Classifier classified uni,clazz,file (%s,%s,%s) as %s",valid,uni,clazz,file,clazzClass);
            if (valid){
                LOG.info(msg);
            }else {
                LOG.warn(msg);
            }
        } while (instanceEnumeration.hasMoreElements());
    }

    public void writeAttributes(File file, Instances dataset) throws Exception {
        String[] attributeNames = new String[dataset.numAttributes()];
        double[] attributeWeights = new double[dataset.numAttributes()];

        for (int i = 0; i < dataset.numAttributes(); i++) {
            Attribute attribute = dataset.attribute(i);
            attributeWeights[i] = attribute.weight();
            attributeNames[i] = attribute.name();
        }

        for (int i = 0; i < dataset.numAttributes(); i++) {
            Attribute attribute = dataset.attribute(i);
            attributeWeights[i] = attribute.weight();
            attributeNames[i] = attribute.name();
        }
        if (file.exists()) {
            file.delete();
        }
        PrintWriter bos = new PrintWriter(new OutputStreamWriter(new FileOutputStream(file)));
        String attributes = "";
        String weights = "";
        String sep = "";
        String separator = "\t";
        for (int i = 0; i < dataset.numAttributes(); i++) {
            attributes += sep + attributeNames[i];
            weights += sep + attributeWeights[i];
            sep = separator;
        }

        bos.println(attributes);
        bos.println(weights);
        bos.flush();
        bos.close();

    }


    public static void main(String[] args) throws Exception {
        LOG.info("Context init");
        ApplicationContext appContext = new AnnotationConfigApplicationContext(AppConfiguration.class);
        App app = appContext.getBean(App.class);
        app.go();
    }
}
