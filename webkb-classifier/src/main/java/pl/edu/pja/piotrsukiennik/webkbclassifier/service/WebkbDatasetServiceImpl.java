package pl.edu.pja.piotrsukiennik.webkbclassifier.service;

import org.apache.log4j.Logger;
import pl.edu.pja.piotrsukiennik.webkbclassifier.DatasetMeta;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.Dataset;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
public class WebkbDatasetServiceImpl implements DatasetService<File> {

    private static final Logger LOG = Logger.getLogger( WebkbDatasetServiceImpl.class );

    private static final int HOW_MANY_PER_UNI = Integer.MAX_VALUE;

    private static final String FILENAME_FILTER_REGEXP = "[_^.]";

    protected void addUniDirToDataset( Dataset<File> dataset, File uniDir, String category ) {
        int i = 0;
        File[] htmlFiles = uniDir.listFiles();
        if ( htmlFiles != null ) {
            String uni = uniDir.getName();
            for ( File f : htmlFiles ) {
                if ( ++i < HOW_MANY_PER_UNI ) {
                    Map<String,String> modifiers = new HashMap<String,String>();
                    String nameFiltered = f.getName().replaceAll(FILENAME_FILTER_REGEXP, "").toLowerCase();
                    modifiers.put(DatasetMeta.ATTRIBUTE_MODIFIER_FILE,nameFiltered);
                    modifiers.put(DatasetMeta.ATTRIBUTE_MODIFIER_UNI,uni);
                    dataset.add( category, f, modifiers );
                }
                else {
                    break;
                }
            }
        }
    }


    protected void addCategoryDirToDataset( Dataset<File> dataset, File categoryDir) {
        File[] uniDirs = categoryDir.listFiles();
        if ( uniDirs != null ) {
            for ( File uniDir : uniDirs ) {
                addUniDirToDataset( dataset, uniDir, categoryDir.getName() );
            }
        }

    }

    public Dataset<File> parse( File dir ) {
        Dataset<File> dataset = new Dataset<File>();
        File[] categoryDirs = dir.listFiles();
        if ( categoryDirs != null ) {
            for ( File categoryDir : categoryDirs ) {
                addCategoryDirToDataset(dataset,categoryDir);
            }
        }
        return dataset;
    }

}
