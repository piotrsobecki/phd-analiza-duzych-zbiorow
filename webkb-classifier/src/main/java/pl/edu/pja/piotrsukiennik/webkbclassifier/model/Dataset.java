package pl.edu.pja.piotrsukiennik.webkbclassifier.model;

import java.util.*;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
public class Dataset<T> {

    private Map<String,DatasetClass<AnnotatedObject<T>>> classes = new HashMap<String, DatasetClass<AnnotatedObject<T>>>();
    private Map<String,Set<String>> modifierValues = new HashMap<String, Set<String>>();

    private int size=0;
    public Dataset() {
    }

    Dataset(Map<String, DatasetClass<AnnotatedObject<T>>> classes) {
        this.classes = classes;
    }

    public Set<String> getClassifications(){
        return classes.keySet();
    }

    public Map<String, DatasetClass<AnnotatedObject<T>>> getClasses() {
        return classes;
    }

    public void add(String classification, T object, Map<String,String> modifiers){
        DatasetClass<AnnotatedObject<T>> clazz = classes.get(classification);
        if (clazz == null ){
            clazz = new DatasetClass<AnnotatedObject<T>>(classification);
            classes.put(classification,clazz);
        }
        clazz.getObjects().add(new AnnotatedObject<T>(object, modifiers));

        for (Map.Entry<String,String> modifier: modifiers.entrySet()){
            Set<String> modVals = modifierValues.get(modifier.getKey());
            if (modVals==null){
                modVals = new HashSet<String>();
                modifierValues.put(modifier.getKey(),modVals);
            }
            modVals.add(modifier.getValue());
        }
        size++;
    }




    public Map<String, Set<String>> getModifierValues() {
        return modifierValues;
    }

    void setModifierValues(Map<String, Set<String>> modifierValues) {
        this.modifierValues = modifierValues;
    }

    public void setClasses(Map<String, DatasetClass<AnnotatedObject<T>>> classes) {
        this.classes = classes;
    }

    void setSize(int size) {
        this.size = size;
    }

    public int getSize() {
        return size;
    }
}
