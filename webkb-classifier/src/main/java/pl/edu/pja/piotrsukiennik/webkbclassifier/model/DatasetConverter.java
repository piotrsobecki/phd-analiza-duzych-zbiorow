package pl.edu.pja.piotrsukiennik.webkbclassifier.model;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import java.util.*;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
public class DatasetConverter<S,T> implements Converter<Dataset<S>,Dataset<T>> {

    private static final Logger LOG = Logger.getLogger( DatasetConverter.class );

    private Converter<S,T> converter;

    public DatasetConverter( Converter<S,T> converter ) {
        this.converter = converter;
    }

    public Dataset<T> convert( Dataset<S> source ) {
        Dataset<T>  ret = new Dataset<T>( convert( source.getClasses() ) );
        ret.setSize(source.getSize());
        ret.setModifierValues(source.getModifierValues());
        return ret;
    }

    public  Map<String,DatasetClass<AnnotatedObject<T>>>  convert(  Map<String,DatasetClass<AnnotatedObject<S>>> source){
        Map<String,DatasetClass<AnnotatedObject<T>>>  target = new HashMap<String, DatasetClass<AnnotatedObject<T>>>();
        for (Map.Entry<String,DatasetClass<AnnotatedObject<S>>> sourceEntry : source.entrySet()){
            DatasetClass<AnnotatedObject<T>> datasetClass = new DatasetClass<AnnotatedObject<T>>(sourceEntry.getKey());
            for (AnnotatedObject<S> annotatedObject: sourceEntry.getValue().getObjects()){
                T targetObject = converter.convert(annotatedObject.getObject());
                datasetClass.getObjects().add(new AnnotatedObject<T>(targetObject,annotatedObject.getModifiers()));
            }
            target.put(sourceEntry.getKey(),datasetClass);
        }
        return target;
    }

}
