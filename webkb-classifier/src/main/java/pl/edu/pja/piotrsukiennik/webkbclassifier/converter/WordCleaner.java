package pl.edu.pja.piotrsukiennik.webkbclassifier.converter;

import opennlp.tools.stemmer.Stemmer;
import opennlp.tools.stemmer.StemmerFactory;
import org.deeplearning4j.text.stopwords.StopWords;
import org.springframework.core.convert.converter.Converter;

import java.util.List;

/**
 * @author Piotr Sukiennik
 * @date 30.05.15
 */
public class WordCleaner implements Converter<String,String> {

    private static final Stemmer STEMMER = StemmerFactory.buildPorterStemmer();

    private static final String CLEANER_REGEXP = "[^a-zA-Z]+";

    private static final List<String> STOP_WORDS = StopWords.getStopWords();


    @Override
    public String convert( String source ) {
        String cleaned = source.replaceAll( CLEANER_REGEXP, "" ).toLowerCase();
        if ( !STOP_WORDS.contains( cleaned ) && !cleaned.isEmpty() ) {
            return STEMMER.stem( cleaned ).toString();
        }
        return null;
    }
}
