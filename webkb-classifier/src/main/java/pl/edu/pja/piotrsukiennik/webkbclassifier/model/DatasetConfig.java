package pl.edu.pja.piotrsukiennik.webkbclassifier.model;

import java.util.Collection;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
public class DatasetConfig {

    private String attributeName;
    private Collection<String> trainSources;
    private Collection<String> testSources;

    public DatasetConfig(String attributeName, Collection<String> trainSources, Collection<String> testSources) {
        this.attributeName = attributeName;
        this.trainSources = trainSources;
        this.testSources = testSources;
    }

    public Collection<String> getTrainSources() {
        return trainSources;
    }

    public Collection<String> getTestSources() {
        return testSources;
    }


    public String getAttributeName() {
        return attributeName;
    }
}
