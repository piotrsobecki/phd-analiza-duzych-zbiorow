package pl.edu.pja.piotrsukiennik.webkbclassifier;

import org.apache.commons.math3.distribution.UniformRealDistribution;

import org.deeplearning4j.models.classifiers.lstm.LSTM;
import org.deeplearning4j.models.featuredetectors.rbm.RBM;
import org.deeplearning4j.nn.api.LayerFactory;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.layers.factory.DefaultLayerFactory;
import org.deeplearning4j.nn.layers.factory.LayerFactories;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.nd4j.linalg.api.activation.Tanh;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import pl.edu.pja.piotrsukiennik.webkbclassifier.converter.DL4JDatasetConverter;
import pl.edu.pja.piotrsukiennik.webkbclassifier.converter.WekaInstancesDatasetConverter;
import pl.edu.pja.piotrsukiennik.webkbclassifier.service.*;
import weka.classifiers.Classifier;
import weka.classifiers.rules.NNge;
import weka.classifiers.trees.J48;
import weka.classifiers.trees.RandomForest;

import java.util.Arrays;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
@Configuration
@ComponentScan(basePackages = "pl.edu.pja.piotrsukiennik.webkbclassifier.*")
@PropertySource("classpath:app.properties")
public class AppConfiguration {

    @Bean
    public Classifier classifier(){
        return new RandomForest();
    }
    @Bean
    public WekaClassifierService wekaClassifierService(){
        return new WekaClassifierServiceImpl(classifier());
    }
    @Bean
    public DL4JDatasetConverter datasetConverter(){
        return new DL4JDatasetConverter();
    }

    @Bean
    public WekaInstancesDatasetConverter wekaInstancesDatasetConverter(){
        return new WekaInstancesDatasetConverter();
    }

    @Bean
    public App app(){
        return new App();
    }

    @Bean
    public DatasetService datasetService() {
        return new WebkbDatasetServiceImpl();
    }

    @Bean
    public ClassifierService classifierService() {
        return new DeepLearningClassifierServiceImpl();
    }

    @Bean
    public HtmlParserService htmlParserService() {
        return new HtmlParserServiceImpl();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
