package pl.edu.pja.piotrsukiennik.webkbclassifier.service;

import org.apache.log4j.Logger;
import pl.edu.pja.piotrsukiennik.webkbclassifier.DatasetMeta;
import weka.attributeSelection.ChiSquaredAttributeEval;
import weka.core.Attribute;
import weka.core.Instances;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Remove;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by Piotr on 2015-07-06.
 */
public class AttributeWeighter {


    private class AttributeCom implements Comparable<AttributeCom>{
        private int index;
        private double weight;

        public AttributeCom(int index, double weight) {
            this.index = index;
            this.weight = weight;
        }

        @Override
        public int compareTo(AttributeCom o) {
            return Double.compare(weight,o.weight);
        }
    }


    private static final Logger LOG = Logger.getLogger(AttributeWeighter.class);

    public void weight(Instances instances) throws Exception{
        ChiSquaredAttributeEval attributeEvaluator = new ChiSquaredAttributeEval();
        attributeEvaluator.buildEvaluator(instances);
        int attrNum = instances.numAttributes();

        for (int i = 0; i < attrNum; i++) {
            Attribute attribute = instances.attribute(i);
            if (attribute.weight()!= DatasetMeta.ESSENTIAL_WEIGHT){
                attribute.setWeight(attributeEvaluator.evaluateAttribute(i));
            }
        }
    }


    public Remove removeWeightlessAttributes(Instances dataset) throws Exception{
        Remove remove = new Remove();
        List<Integer> attributesToRemove = new ArrayList<Integer>();
        for (int i =0; i<dataset.numAttributes(); i++){
            Attribute attribute = dataset.attribute(i);
            if (attribute.weight()==0){
                attributesToRemove.add(i);
            }
        }
        int[] attrArray = new int[attributesToRemove.size()];
        for (int i=0; i<attributesToRemove.size(); i++){
            attrArray[i] = attributesToRemove.get(i);
        }
        remove.setAttributeIndicesArray(attrArray);
        remove.setInputFormat(dataset);
        return remove;
    }


    public Remove onlyTopAttributes(Instances dataset, double ratio) throws Exception{
        Remove remove = new Remove();
        List<AttributeCom> attrSorted = new ArrayList<AttributeCom>();
        for (int i =0; i<dataset.numAttributes(); i++){
            Attribute attribute = dataset.attribute(i);
            double weight = attribute.weight();
            if (weight!= DatasetMeta.ESSENTIAL_WEIGHT){
                attrSorted.add(new AttributeCom(i, attribute.weight()));
            }
        }
        Collections.sort(attrSorted);

        int numAttr = attrSorted.size();
        int attrsToRemove = (int) Math.max(0, Math.min(Math.ceil(numAttr * ratio), numAttr));
        List<Integer> toRemove = new ArrayList<Integer>();
        for (int i = 0; i<attrsToRemove;i++ ){
            toRemove.add(attrSorted.get(i).index);
        }
        int[] attrArray = new int[toRemove.size()];
        for (int i=0; i<toRemove.size(); i++){
            attrArray[i] = toRemove.get(i);
        }
        remove.setAttributeIndicesArray(attrArray);
        remove.setInputFormat(dataset);
        return remove;
    }
}
