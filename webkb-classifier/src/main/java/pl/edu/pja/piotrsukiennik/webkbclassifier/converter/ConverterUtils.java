package pl.edu.pja.piotrsukiennik.webkbclassifier.converter;

import org.apache.log4j.Logger;
import org.springframework.core.convert.converter.Converter;

import java.util.Collection;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
public class ConverterUtils {


    private static final Logger LOG = Logger.getLogger( ConverterUtils.class );


    private static <T> T notSafeNewInstance(Class<T> clazz){
        try {
            return clazz.newInstance();
        }
        catch ( InstantiationException e ) {
           LOG.error( "Clazz new instance exception",e );
            throw new RuntimeException( e );
        }
        catch ( IllegalAccessException e ) {
           LOG.error( "Clazz new instance exception", e );
            throw new RuntimeException( e );
        }
    }

    public static <S,T> Collection<T> convertAll(Collection<S> data, Converter<S,T> converter){
        Collection<T> outData= notSafeNewInstance( data.getClass() );
        for (S dat: data){
            outData.add( converter.convert( dat ) );
        }
        return outData;
    }

}
