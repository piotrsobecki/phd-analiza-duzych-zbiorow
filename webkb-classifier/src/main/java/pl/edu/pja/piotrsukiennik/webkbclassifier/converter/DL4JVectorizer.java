package pl.edu.pja.piotrsukiennik.webkbclassifier.converter;

import org.deeplearning4j.models.word2vec.wordstore.VocabCache;
import org.deeplearning4j.text.tokenization.tokenizer.Tokenizer;
import org.deeplearning4j.text.tokenization.tokenizerfactory.DefaultTokenizerFactory;
import org.deeplearning4j.text.tokenization.tokenizerfactory.TokenizerFactory;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.util.FeatureUtil;
import org.springframework.core.convert.converter.Converter;
import pl.edu.pja.piotrsukiennik.webkbclassifier.model.AnnotatedObject;

import java.util.List;

/**
 * @author Piotr Sukiennik
 * @date 30.05.15
 */
public class DL4JVectorizer {

    private static final TokenizerFactory TOKENIZER_FACTORY = new DefaultTokenizerFactory();

    private static final Converter<String, String> CLEANER = new WordCleaner();

    private VocabCache vocabCache;

    public DL4JVectorizer(VocabCache vocabCache) {
        this.vocabCache = vocabCache;
    }

    protected void addWordToDocumentVector( VocabCache vocabCache, INDArray input, String token ) {
        String cleanToken = CLEANER.convert( token );
        if ( cleanToken != null && !cleanToken.isEmpty() ) {
            int tokenIndex = vocabCache.indexOf( cleanToken );
            if ( tokenIndex > 0 ) {
                input.putScalar( tokenIndex, input.getInt( tokenIndex ) + 1 );
            }
        }

    }

    public INDArray documentVector( VocabCache vocabCache, AnnotatedObject<List<String>> annotatedObject ) {
        INDArray input = Nd4j.create( 1, vocabCache.numWords() );
        for ( String string : annotatedObject.getObject() ) {
            Tokenizer tokenizer = TOKENIZER_FACTORY.create( string );
            for ( String token : tokenizer.getTokens() ) {
                addWordToDocumentVector( vocabCache, input, token );
            }
        }
        return input;
    }

    public INDArray labelVector( List<String> labels, String label ) {
        return FeatureUtil.toOutcomeVector( labels.indexOf( label ), labels.size() );
    }

    public VocabCache getVocabCache() {
        return vocabCache;
    }
}
