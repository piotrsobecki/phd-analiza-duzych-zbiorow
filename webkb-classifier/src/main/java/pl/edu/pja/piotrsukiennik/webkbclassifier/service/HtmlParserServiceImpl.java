package pl.edu.pja.piotrsukiennik.webkbclassifier.service;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Element;
import java.io.File;
import java.io.IOException;

/**
 * @author Piotr Sukiennik
 * @date 23.05.15
 */
public class HtmlParserServiceImpl implements HtmlParserService {

    public Document parseDocument( File file ) throws IOException{
       return Jsoup.parse( file, "UTF-8" );
    }

    public Elements getElementsWithText( Document doc ) {
        return doc.select(":not(:empty)");
    }

    public String getTextFromElement(Element element){
        return element.data();
    }
}
