package opennlp.tools.stemmer;

/**
 * @author Piotr Sukiennik
 * @date 30.05.15
 */
public class StemmerFactory {


    private StemmerFactory(){

    }

    public static Stemmer buildPorterStemmer(){
        return new PorterStemmer();
    }
}
